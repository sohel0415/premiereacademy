<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $role = new \App\Model\Role();
        $role->slug = 'user';
        $role->name = 'User';
        $role->save();

        $role = new \App\Model\Role();
        $role->slug = 'admin';
        $role->name = 'Admin';
        $role->save();

        $this->command->info('Role model seeded!');
    }
}
