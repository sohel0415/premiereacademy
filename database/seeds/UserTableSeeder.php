<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $user = new \App\Model\User();
        $user->username = 'admin';
        $user->email = 'admin@admin.com';
        $user->phone = '01751024502';
        $user->password = Hash::make('admin');
        $user->isSocial = false;
        $user->isActive = true;
        $user->save();

        $role = \App\Model\Role::where('slug','admin')->first();
        $role->users()->attach($user);

        $this->command->info('User model seeded!');
    }
}
