<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Question;
use App\Model\Test;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return view('admin.question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tests = Test::all();

        return view('admin.question.create', compact('tests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();

        $question = new Question();
        $question->test_id = $request->get('test_id');
        $question->question_description = $request->get('question_description');
        $question->option1 = $request->get('option1');
        $question->option2 = $request->get('option2');
        $question->option3 = $request->get('option3');
        $question->option4 = $request->get('option4');
        $question->answer = $request->get('answer');
        $question->save();

        return redirect('/paadmin/question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tests = Test::all();
        $question = Question::find($id);

        return view('admin.question.edit', compact(['tests','question']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::find($id);
        $question->test_id = $request->get('test_id');
        $question->question_description = $request->get('question_description');
        $question->option1 = $request->get('option1');
        $question->option2 = $request->get('option2');
        $question->option3 = $request->get('option3');
        $question->option4 = $request->get('option4');
        $question->answer = $request->get('answer');
        $question->save();

        return redirect('/paadmin/question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::where('id', $id)->delete();

        return redirect('/paadmin/question');
    }
}
