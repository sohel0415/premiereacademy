<?php
/**
 * Created by PhpStorm.
 * User: sohel0415
 * Date: 8/18/2018
 * Time: 3:43 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(){
        return view('admin.dashboard');
    }
}