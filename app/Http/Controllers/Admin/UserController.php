<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $users = User::orderBy('id','desc')->get();

        return view('admin.user.index', compact('users'));
    }

    public function activate($id){
        $user = User::find($id);

        if($user!=null){
            $user->isActive = true;
            $user->save();
        }

        return redirect('/paadmin/users')->with('successMessage', 'User activated');
    }
}