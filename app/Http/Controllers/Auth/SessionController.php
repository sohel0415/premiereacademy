<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginValidation;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Socialite;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function signin()
    {
        return view('auth.signin');
    }

    public function postSignin(LoginValidation $loginValidation)
    {

        $loginValidation->all();
        $attempt = Auth::attempt([
            'username'=>$loginValidation->get('username'),
            'password' => $loginValidation->get('password'),
            'isSocial' => 0,
            'isActive' => 1
        ]);

        if($attempt) return redirect()->intended('/dashboard');

        return redirect()->back()->with('errorMessage','Invalid Credentials!!');
    }
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);

        return redirect('/dashboard');
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->where('isSocial',1)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'provider' => $provider,
            'provider_id' => $user->id,
            'isSocial' => 1,
            'isActive' => 1
        ]);
    }

    public function logout()
    {
        Auth::logout();

        return redirect('signin');
    }
}
