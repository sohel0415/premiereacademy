<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\PracticeSession;
use App\Model\Question;
use App\Model\Result;
use App\Model\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PracticeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function practiceTest($category)
    {
        $maths = Test::where('category', $category)->where('subject', 'math')->get();
        $verbals = Test::where('category', $category)->where('subject', 'verbal')->get();

        return view('user.practice_test_home', compact(['category', 'maths', 'verbals']));
    }

    public function startPracticeTest($test_id)
    {
        $practice_session = PracticeSession::firstOrNew(['user_id' => \Auth::user()->id, 'test_id' => $test_id]);
        $practice_session->endtime = time()+(30*60);
        $practice_session->save();

        Result::where(['user_id' => \Auth::user()->id, 'test_id' => $test_id])->delete();

        return redirect('/practice_test/session/' . $test_id);
    }

    public function sessionPracticeTest($test_id)
    {
        $questions = Question::where('test_id', $test_id)->paginate(1);

        $practice_session = PracticeSession::where(['user_id' => \Auth::user()->id, 'test_id' => $test_id])->first();

        return view('user.practice_test_session', compact(['questions','practice_session']));
    }

    public function postSessionPracticeTest(Request $request, $question_id)
    {
        //return $request->all();
        $question = Question::find($question_id);

        $result = Result::firstOrNew(['user_id' => \Auth::user()->id, 'test_id' => $question->test_id, 'question_id' => $question->id]);
        $answer = $request->get('answer');
        $result->answer = $answer;

        if ($answer == $question->answer)
            $result->isCorrect = true;
        else
            $result->isCorrect = false;

        $result->save();

        if($request->get('button')=='prev'){
            return redirect($request->get('prev'));
        }
        else if($request->get('button')=='next')
            return redirect($request->get('next'));

        return redirect('/dashboard');
    }

    public function checkResults($test_id)
    {
        $questions = Question::where('test_id', $test_id)->paginate(1);

        return view('user.check_results', compact(['questions']));
    }
}
