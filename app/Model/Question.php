<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'test_id','question_description', 'option1', 'option2','option3', 'option4','answer'
    ];

    public function tests()
    {
        return $this->belongsTo(Test::class,'test_id','id');
    }
}