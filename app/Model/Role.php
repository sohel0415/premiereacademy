<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'slug','name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class,'role_users','role_id','user_id');
    }
}