<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';

    protected $fillable = [
        'user_id','test_id', 'question_id', 'answer', 'isCorrect'
    ];

    public function users()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function tests()
    {
        return $this->belongsTo(Test::class,'test_id','id');
    }

    public function questions()
    {
        return $this->belongsTo(Question::class,'question_id','id');
    }
}