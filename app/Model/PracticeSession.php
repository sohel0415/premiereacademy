<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PracticeSession extends Model
{
    protected $table = 'practice_session';

    protected $fillable = [
        'user_id', 'test_id', 'endtime'
    ];

    public function users()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function tests()
    {
        return $this->belongsTo(Test::class,'test_id','id');
    }
}