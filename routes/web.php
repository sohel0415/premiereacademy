<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/signup','Auth\RegistrationController@signup');
Route::post('/signup','Auth\RegistrationController@postSignup');
Route::get('/auth/verify','Auth\RegistrationController@verify');

Route::get('/signin', [ 'as' => 'login', 'uses' => 'Auth\SessionController@signin']);
Route::post('/signin','Auth\SessionController@postSignin');
Route::get('auth/{provider}', 'Auth\SessionController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\SessionController@handleProviderCallback');

Route::group(['middleware' => 'auth'],function() {
    Route::get('/logout', 'Auth\SessionController@logout');

    Route::get('/dashboard', 'User\DashboardController@dashboard');

    Route::get('/lesson', 'User\LessonController@index');

    Route::get('/practice_test/{category}', 'User\PracticeController@practiceTest');
    Route::get('/practice_test/start/{test_id}', 'User\PracticeController@startPracticeTest');
    Route::get('/practice_test/session/{test_id}', 'User\PracticeController@sessionPracticeTest');
    Route::post('/practice_test/session/{question_id}', 'User\PracticeController@postSessionPracticeTest');

    Route::get('/practice_test/check_results/{test_id}', 'User\PracticeController@checkResults');

    Route::group(['prefix' => 'paadmin'],function(){
        Route::get('/dashboard','Admin\DashboardController@dashboard');
        Route::resource('test','Admin\TestController');
        Route::resource('question','Admin\QuestionController');
        Route::get('/users','Admin\UserController@index');
        Route::post('/users/activate/{id}','Admin\UserController@activate');
    });

});
