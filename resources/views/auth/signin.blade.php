@extends('layout.layout')

@section('title')
    <title>Signin</title>
@endsection

@section('stylesheet')
    <style>
        .page-content--bge5 {
            height: 100vh;
        }
    </style>
@endsection

@section('content')
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="{{url('/')}}">
                            Premiere Academy
                        </a>
                    </div>
                    <div class="login-form">
                        {{Form::open(array('url'=>'/signin'))}}
                            <div class="form-group">
                                <label>Username</label>
                                <input class="au-input au-input--full" type="text" name="username" placeholder="username" value="{{\Illuminate\Support\Facades\Input::old('username')}}" required>
                                <span class="validator_output <?php if($errors->first('username')!=null) echo "alert-danger"?>">{{ $errors->first('username') }}</span>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                <span class="validator_output <?php if($errors->first('password')!=null) echo "alert-danger"?>">{{ $errors->first('password') }}</span>
                            </div>
                            <div class="login-checkbox">
                                <label>
                                    <input type="checkbox" name="remember">Remember Me
                                </label>
                                <label>
                                    <a href="#">Forgotten Password?</a>
                                </label>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>

                        {{Form::close()}}
                        <div class="register-link">
                            <p>
                                Don't you have account?
                                <a href="{{url('/signup')}}">Sign Up Here</a>
                            </p>
                            <p>
                                Sign in with
                                <a href="{{ url('/auth/github') }}"><i class="fa fa-github"></i> Github</a>
                                {{--<a href="{{ url('/auth/github') }}"><i class="fa fa-github"></i> Github</a>--}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection