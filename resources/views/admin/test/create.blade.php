@extends('layout.base_admin')

@section('title')
    <title>Test : Create</title>
@endsection

@section('stylesheet')

@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Test : Create</h3>

                    <div class="login-form col-md-8" style="margin: auto">
                        {{Form::open(array('url'=>'/paadmin/test'))}}
                        <div class="form-group">
                            <label>Test Name</label>
                            <input class="au-input au-input--full" type="text" name="test_name" placeholder="test name" value="{{\Illuminate\Support\Facades\Input::old('test_name')}}" required>
                            <span class="validator_output <?php if($errors->first('test_name')!=null) echo "alert-danger"?>">{{ $errors->first('test_name') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <select class="form-control" name="category">
                                <option value="">Select Category</option>
                                <option value="homework">Homework</option>
                                <option value="drill">Drill</option>
                                <option value="practice">Practice</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Subject Name</label>
                            <select class="form-control" name="subject">
                                <option value="">Select Subject</option>
                                <option value="math">Math</option>
                                <option value="verbal">Verbal</option>
                            </select>
                        </div>
                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Create</button>

                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')

@endsection
