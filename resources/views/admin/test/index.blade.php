@extends('layout.base_admin')

@section('title')
    <title>Test</title>
@endsection

@section('stylesheet')

@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Test <a href="{{ url('/paadmin/test/create') }}" style="float: right">Add</a></h3>

                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Test Name</th>
                                <th>Category</th>
                                <th>Subject Name</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tests as $test)
                            <tr class="tr-shadow">
                                <td>{{$test->test_name}}</td>
                                <td>{{$test->category}}</td>
                                <td>{{$test->subject}}</td>
                                <td>
                                    <span class="status--process"><a href="{{url('/paadmin/test/'.$test->id.'/edit')}}"><i class="fa fa-edit"></i></a></span>
                                    <span class="status--process">
                                        <form method="POST" id="deleteForm" action="{{url('/paadmin/test/'.$test->id)}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="delete"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')

@endsection