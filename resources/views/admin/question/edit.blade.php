@extends('layout.base_admin')

@section('title')
    <title>Question : Edit</title>
@endsection

@section('stylesheet')
    <script src="{{asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script>tinymce.init({ selector:'textarea'});</script>
@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Question : Edit</h3>

                    <div class="login-form col-md-8" style="margin: auto">
                        {{Form::open(array('url'=>'/paadmin/question/'.$question->id,'method'=>'PUT'))}}
                        <div class="form-group">
                            <label>Test Name</label>
                            <select class="form-control" name="test_id">
                                <option value="">Select Test</option>
                                @foreach($tests as $test)
                                    <option value="{{$test->id}}" @if($test->id==$question->test_id) selected @endif>{{$test->test_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Question Description</label>
                            <textarea name="question_description">{{\Illuminate\Support\Facades\Input::old('question_description', $question->question_description)}}</textarea>
                            <span class="validator_output <?php if($errors->first('question_description')!=null) echo "alert-danger"?>">{{ $errors->first('question_description') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Option1</label>
                            <input class="au-input au-input--full" type="text" name="option1" placeholder="option1" value="{{\Illuminate\Support\Facades\Input::old('option1', $question->option1)}}" required>
                            <span class="validator_output <?php if($errors->first('option1')!=null) echo "alert-danger"?>">{{ $errors->first('option1') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Option2</label>
                            <input class="au-input au-input--full" type="text" name="option2" placeholder="option2" value="{{\Illuminate\Support\Facades\Input::old('option2', $question->option2)}}" required>
                            <span class="validator_output <?php if($errors->first('option2')!=null) echo "alert-danger"?>">{{ $errors->first('option2') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Option3</label>
                            <input class="au-input au-input--full" type="text" name="option3" placeholder="option3" value="{{\Illuminate\Support\Facades\Input::old('option3', $question->option3)}}" required>
                            <span class="validator_output <?php if($errors->first('option3')!=null) echo "alert-danger"?>">{{ $errors->first('option3') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Option4</label>
                            <input class="au-input au-input--full" type="text" name="option4" placeholder="option4" value="{{\Illuminate\Support\Facades\Input::old('option4', $question->option4)}}" required>
                            <span class="validator_output <?php if($errors->first('option4')!=null) echo "alert-danger"?>">{{ $errors->first('option4') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Answer</label>
                            <select class="form-control" name="answer">
                                <option value="option1" @if($question->answer=="option1") selected @endif>Option1</option>
                                <option value="option2" @if($question->answer=="option2") selected @endif>Option2</option>
                                <option value="option3" @if($question->answer=="option3") selected @endif>Option3</option>
                                <option value="option4" @if($question->answer=="option4") selected @endif>Option4</option>
                            </select>
                            <span class="validator_output <?php if($errors->first('answer')!=null) echo "alert-danger"?>">{{ $errors->first('answer') }}</span>
                        </div>
                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Create</button>

                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')

@endsection
