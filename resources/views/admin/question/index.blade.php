@extends('layout.base_admin')

@section('title')
    <title>Question</title>
@endsection

@section('stylesheet')

@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Question <a href="{{ url('/paadmin/question/create') }}" style="float: right">Add</a></h3>

                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Test Name</th>
                                <th>Question Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                            <tr class="tr-shadow">
                                <td>{{$question->tests->test_name}}</td>
                                <td>{!!$question->question_description!!}</td>
                                <td>
                                    <span class="status--process"><a href="{{url('/paadmin/question/'.$question->id.'/edit')}}"><i class="fa fa-edit"></i></a></span>
                                    <span class="status--process">
                                        <form method="POST" id="deleteForm" action="{{url('/paadmin/question/'.$question->id)}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="delete"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')

@endsection