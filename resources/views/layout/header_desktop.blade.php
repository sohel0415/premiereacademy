<!-- HEADER DESKTOP-->
<header class="header-desktop3 d-none d-lg-block">
    <div class="section__content section__content--p35">
        <div class="header3-wrap">
            <div class="header__logo">
                <a href="{{url('/')}}">
                    Premiere Academy
                </a>
            </div>
            <div class="header__navbar">
                <ul class="list-unstyled">
                    <li>
                        <a href="{{url('/dashboard')}}">
                            <i class="fas fa-tachometer-alt"></i>Dashboard
                            <span class="bot-line"></span>
                        </a>
                    </li>

                    <li class="has-sub">
                        <a href="#">
                            <i class="fas fa-copy"></i>
                            <span class="bot-line"></span>Exercise</a>
                        <ul class="header3-sub-list list-unstyled">
                            <li>
                                <a href="{{url('/practice_test/homework')}}">Homework</a>
                            </li>
                            <li>
                                <a href="{{url('/practice_test/drill')}}">Drill</a>
                            </li>
                            <li>
                                <a href="{{url('/practice_test/practice')}}">Practice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('/lesson')}}">
                            <i class="fas fa-tachometer-alt"></i>Lesson
                            <span class="bot-line"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="content">
                            <a class="js-acc-btn" href="#">{{ Auth::user()->username }}</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
                                <a href="{{url('/logout')}}">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- END HEADER DESKTOP-->