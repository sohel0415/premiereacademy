<!-- HEADER DESKTOP-->
<header class="header-desktop3 d-none d-lg-block">
    <div class="section__content section__content--p35">
        <div class="header3-wrap">
            <div class="header__logo">
                <a href="{{url('/')}}">
                    Premiere Academy
                </a>
            </div>
            <div class="header__navbar">
                <ul class="list-unstyled">
                    <li>
                        <a href="{{url('/paadmin/dashboard')}}">
                            <i class="fas fa-tachometer-alt"></i>Dashboard
                            <span class="bot-line"></span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/paadmin/users')}}">
                            <i class="fa fa-user"></i>User
                            <span class="bot-line"></span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/paadmin/test')}}">
                            <i class="fas fa-tachometer-alt"></i>Test
                            <span class="bot-line"></span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/paadmin/question')}}">
                            <i class="fas fa-tachometer-alt"></i>Question
                            <span class="bot-line"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="content">
                            <a class="js-acc-btn" href="#">{{ Auth::user()->username }}</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
                                <a href="{{url('/logout')}}">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- END HEADER DESKTOP-->