@extends('layout.base')

@section('title')
    <title>Exercise : {{$category}}</title>
@endsection

@section('stylesheet')

@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title-5 m-b-35" style="text-align: center">Math</h3>

                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Test Name</th>
                                <th>Previous Result</th>
                                <th>Give This Test A Try</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($maths as $math)
                            <tr class="tr-shadow">
                                <td>{{$math->test_name}}</td>
                                <td class="desc"><a href="{{url('/practice_test/check_results/'.$math->id)}}"> Check Result</a></td>
                                <td>
                                    <span class="status--process"><a href="{{url('/practice_test/start/'.$math->id)}}"> Go To Question </a></span>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 class="title-5 m-b-35" style="text-align: center">Verbal</h3>

                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Test Name</th>
                                <th>Previous Result</th>
                                <th>Give This Test A Try</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($verbals as $verbal)
                                <tr class="tr-shadow">
                                    <td>{{$verbal->test_name}}</td>
                                    <td class="desc"><a href="{{url('/practice_test/check_results/'.$verbal->id)}}"> Check Result</a></td>
                                    <td>
                                        <span class="status--process"><a href="{{url('/practice_test/start/'.$verbal->id)}}"> Go To Question </a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')

@endsection