@extends('layout.base')

@section('title')
    <title>Practice Test</title>
@endsection

@section('stylesheet')

@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Practice Test <p style="float: right" id="demo2"></p></h3>
                </div>
                @foreach($questions as $question)
                    <div class="col-sm-8">
                        {!! $question->question_description !!}
                    </div>
                    <div class="login-form col-sm-4">
                        {{Form::open(array('url'=>'/practice_test/session/'.$question->id))}}
                            <h6 class="title-5 m-b-20">Select Answer</h6>
                         <?php $result = \App\Model\Result::where(['user_id' => \Auth::user()->id, 'test_id' => $question->test_id, 'question_id' => $question->id])->first();?>
                            <input type="radio" name="answer" value="option1" @if("option1"==optional($result)->answer) checked @endif> {{$question->option1}}<br>
                            <input type="radio" name="answer" value="option2" @if("option2"==optional($result)->answer) checked @endif> {{$question->option2}}<br>
                            <input type="radio" name="answer" value="option3" @if("option3"==optional($result)->answer) checked @endif>  {{$question->option3}}<br>
                            <input type="radio" name="answer" value="option4" @if("option4"==optional($result)->answer) checked @endif>  {{$question->option4}}<br>

                            <div class="col-md-12">
                                @if($questions->previousPageUrl()!=null)
                                    <button type="submit" name="button" value="prev" class="btnMarginTop au-btn au-btn--green m-b-10">Prev</button>
                                @endif
                                @if($questions->hasMorePages())
                                    <button type="submit" name="button" value="next" class="btnMarginTop au-btn au-btn--green m-b-10">Next</button>
                                @else
                                    <button type="submit" name="button" value="finish" class="btnMarginTop au-btn au-btn--green m-b-10">Finish Test</button>
                                @endif
                            </div>
                        <input type="hidden" name="prev" value="{{$questions->previousPageUrl()}}">
                        <input type="hidden" name="next" value="{{$questions->nextPageUrl()}}">
                        {{Form::close()}}
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')
    <script>
        // Set the date we're counting down to
        var countDownDate = new Date(<?php echo $practice_session->endtime; ?>*1000).getTime();

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = hours + "h "
            + minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>
@endsection