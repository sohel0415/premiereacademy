@extends('layout.base')

@section('title')
    <title>Practice Test</title>
@endsection

@section('stylesheet')

@endsection

@section('content_body')
    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Practice Test <p style="float: right" id="demo"></p></h3>
                </div>
                @foreach($questions as $question)
                    <div class="col-sm-8">
                        {!! $question->question_description !!}
                    </div>
                    <div class="login-form col-sm-4">
                        <h6 class="title-5 m-b-20">Select Answer</h6>
                        <?php $result = \App\Model\Result::where(['user_id' => \Auth::user()->id, 'test_id' => $question->test_id, 'question_id' => $question->id])->first();?>
                        <div @if($question->answer=="option1") class="correctAnswer" @endif>
                            <input type="radio" name="answer" value="option1" @if("option1"==optional($result)->answer)
                                   checked @endif> {{$question->option1}}<br>
                        </div>
                        <div @if($question->answer=="option2") class="correctAnswer" @endif>
                            <input type="radio" name="answer" value="option2" @if("option2"==optional($result)->answer)
                                   checked @endif> {{$question->option2}}<br>
                        </div>
                        <div @if($question->answer=="option3") class="correctAnswer" @endif>
                            <input type="radio" name="answer" value="option3" @if("option3"==optional($result)->answer)
                                   checked @endif>  {{$question->option3}}<br>
                        </div>
                        <div @if($question->answer=="option4") class="correctAnswer" @endif>
                            <input type="radio" name="answer" value="option4" @if("option4"==optional($result)->answer)
                                   checked @endif>  {{$question->option4}}<br>
                        </div>
                        <div class="col-md-12">
                            @if($questions->previousPageUrl()!=null)
                                <button type="button" class="btnMarginTop au-btn au-btn--green m-b-10"><a
                                            href="{{$questions->previousPageUrl()}}">Prev</a></button>
                            @endif
                            @if($questions->hasMorePages())
                                <button type="button" class="btnMarginTop au-btn au-btn--green m-b-10"><a
                                            href="{{$questions->nextPageUrl()}}">Next</a></button>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->

@endsection

@section('script')

@endsection